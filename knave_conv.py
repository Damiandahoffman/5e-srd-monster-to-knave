import requests
import json
import numpy as np
import re


def nice_print(name):
    knave_dict = knave_convert(name)
    text = "" + knave_dict['name'] + "\n"
    if knave_dict["HD"] is not None:
        text += "HD:" + str(knave_dict['HD']) + ";AD:" + str(knave_dict['AD']) + ";HP:" + str(knave_dict['HP']) + \
                ";ML:" + str(knave_dict['ML']) + ";SP:" + str(knave_dict['SP']) + "\n"
        if knave_dict["Actions"] is not None:
            if "MA" in knave_dict["Actions"]:
                text += "\n" + knave_dict["Actions"]["MA"] + "\n"
            for action in knave_dict["Actions"]["Attack"]:
                text += "\n" + action + "\n"
        if "DC" in knave_dict["Magic"]:
            text += "Spells (DC:" + str(knave_dict["Magic"]["DC"]) + "): \n"
            for spell in knave_dict["Magic"]["spells"]:
                text += spell + ";"
    text += "\n"
    return text


def knave_convert(name):
    knave_dict = dict()
    response = requests.get("https://www.dnd5eapi.co/api/monsters/" + name.lower())
    if response.status_code == 200:
        response_dict = json.loads(response.text)
        knave_dict['name'] = response_dict["name"]
        knave_dict['HD'] = CR2HD(response_dict["challenge_rating"])
        knave_dict['SP'] = movement2speed(response_dict["speed"])
        knave_dict['HP'] = max(int(max(4 * knave_dict['HD'], 1) + stat2mod(int(response_dict["constitution"]))), 1)
        knave_dict['AD'] = response_dict["armor_class"]
        knave_dict['ML'] = wisdom2morale(stat2mod(response_dict["wisdom"]))
        knave_dict["Actions"] = getattackactions(response_dict)
        knave_dict['Magic'] = getspellList(response_dict)
        return knave_dict
    knave_dict['name'] = 'not found'


def CR2PROF(CR):
    return np.ceil(0.25 * CR + 1)


def CR2HD(CR):
    return int(np.ceil(CR))


def stat2mod(stat):
    return np.floor(stat / 2) - 5


def wisdom2morale(wisdom):
    return int(7 + np.floor(wisdom / 2.5))


def movement2speed(movment):
    max_speed = 0
    for movment_type in movment:
        try:
            max_speed = max(max_speed, int(movment[movment_type].split()[0]))
        except:
            max_speed = max_speed
    return max_speed


def getspellList(response_dict):
    Magic = dict()
    spells = list()
    if ("special_abilities" in response_dict):
        for x in response_dict["special_abilities"]:
            if "spellcasting" in x["name"].lower():
                if "dc" in x["spellcasting"]:
                    Magic["DC"] = x["spellcasting"]["dc"]
                    for s_spell in x["spellcasting"]["spells"]:
                        spells.append(s_spell["name"] + "(" + str(s_spell["level"]) + ")")

    Magic["spells"] = spells
    return Magic


def getallmosters():
    response = requests.get("https://www.dnd5eapi.co/api/monsters/")
    if (response.status_code != 200):
        print('not found')
        return
    response_dict = json.loads(response.text)
    names = list()
    index = list()
    for item in response_dict['results']:
        names.append((item["name"]))
        index.append(item["index"])
    return names, index


def getattackactions(response_dict):
    action_dict = dict()
    action_dict["Attack"] = list()
    if "actions" in response_dict:
        if (response_dict["actions"][0]["name"] == "Multiattack"):
            action_dict["MA"] = response_dict["actions"][0]["desc"]
            action_start_idx = 1
        else:
            action_start_idx = 0
        action_dict["Attack"] = [None] * (len(response_dict["actions"]) - action_start_idx)
        for idx in range(action_start_idx, len(response_dict["actions"])):
            action = response_dict["actions"][idx]
            if (len(action['damage']) < 1):
                action_dict["Attack"][idx - action_start_idx] = action["name"] + ":" + str(
                    action["desc"])
                continue

            if (not "attack_bonus" in action):
                action_dict["Attack"][idx - action_start_idx] = action["name"] + ":" + str(
                    action["desc"])
            else:
                action_dict["Attack"][idx - action_start_idx] = action["name"] + ": " + str(
                    action["attack_bonus"] + 10) + " "

            for damage_type in action["damage"]:
                if ("from" in damage_type):
                    damage_type = damage_type['from'][0]
                av_damage = getaveragedamage(damage_type['damage_dice'])
                action_dict["Attack"][idx - action_start_idx] += damage_type['damage_type']['name'] + " " + "(" + str(
                    int(av_damage)) + ") "
        return action_dict


def getaveragedamage(damage_string):
    die_match = re.findall("([0-9]+)d([0-9]+)(.)([0-9]+)", damage_string)
    if len(die_match) == 0:
        die_match = re.findall("([0-9]+)d([0-9]+)", damage_string)
        if len(die_match) == 0:
            av_damage = int(damage_string)
            return av_damage
    av_damage = int(die_match[0][0]) * (int(die_match[0][1]) / 2)
    return av_damage
