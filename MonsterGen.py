from knave_conv import nice_print, requests, json
import random
import sys

CR = sys.argv[1]
response = requests.get("https://www.dnd5eapi.co/api/monsters/?challenge_rating=" + str(CR))
response_dict = json.loads(response.text)
selected_idx = random.randint(0, response_dict['count']-1)
print(nice_print(response_dict['results'][selected_idx]['index']))
