import PySimpleGUI as sg

from knave_conv import getallmosters, nice_print


def gui_init():
    names, index = getallmosters()
    selector = [
                    [sg.Input(size=(20, 1), enable_events=True, key='-INPUT-')],
                    [sg.Listbox(names, size=(20, 30), enable_events=True, key='-LIST-')],
               ]
    display = [
                    [sg.Text(size=(30, 40), key="-TOUT-")]
              ]

    layout = [
                [
                    [sg.Text('SRD monster search')],
                    [sg.Column(selector),
                     sg.VSeperator(),
                     sg.Column(display, scrollable=False, vertical_scroll_only=True)],
                 ]
            ]
    window = sg.Window('SRD monster knave', layout)
    return window
    # Event Loop


def gui_run(window):
    names, index = getallmosters()
    while True:
        event, values = window.read()
        if event in (sg.WIN_CLOSED, 'Exit'):  # always check for closed window
            break
        if values['-INPUT-'] != '':  # if a keystroke entered in search field
            search = values['-INPUT-']
            new_values = [x for x in names if search.upper() in x.upper()]  # do the filtering
            window['-LIST-'].update(new_values)  # display in the listbox
        else:
            # display original unfiltered list
            window['-LIST-'].update(names)
        # if a list item is chosen
        if event == '-LIST-' and len(values['-LIST-']):
            call_name = index[names.index(values['-LIST-'][0])]
            window["-TOUT-"].update((nice_print(call_name)))


if __name__ == '__main__':
    window =  gui_init()
    gui_run(window)